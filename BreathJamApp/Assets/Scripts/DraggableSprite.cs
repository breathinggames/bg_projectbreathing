﻿using UnityEngine;
using System.Collections;

public class DraggableSprite : MonoBehaviour
{
    float x;
    float y;

    public float minSwipeDist, maxSwipeTime, swipeStartTime;
    bool couldBeSwipe;
    Vector2 startPos;
    public Sprite Disappearer;

    // Use this for initialization
    void Start()
    {
        StartCoroutine(checkHorizontalSwipes());
    }

    // Update is called once per frame
    void Update()
    {
        foreach (Touch touch in Input.touches)
        {
            x = touch.position.x;
            y = touch.position.y;
        }
        x = Input.mousePosition.x;
        y = Input.mousePosition.y;
    }

    void OnMouseDrag()
    {
        transform.position = Camera.main.ScreenToWorldPoint(new Vector3(x, y, 10.0f));
    }

    void OnTouchDrag()
    {
        transform.position = Camera.main.ScreenToWorldPoint(new Vector3(x, y, 10.0f));
    }

    IEnumerator checkHorizontalSwipes() //Coroutine, wich gets Started in "Start()" and runs over the whole game to check for swipes
    {
        while (true)
        { //Loop. Otherwise we wouldnt check continoulsy ;-)
            foreach (Touch touch in Input.touches)
            { //For every touch in the Input.touches - array...

                switch (touch.phase)
                {
                    case TouchPhase.Began: //The finger first touched the screen --> It could be(come) a swipe
                        couldBeSwipe = true;

                        startPos = touch.position;  //Position where the touch started
                        swipeStartTime = Time.time; //The time it started
                        break;

                    case TouchPhase.Stationary: //Is the touch stationary? --> No swipe then!
                        couldBeSwipe = false;
                        break;
                }
                float swipeTime = Time.time - swipeStartTime; //Time the touch stayed at the screen till now.
                float swipeDist = Mathf.Abs(touch.position.x - startPos.x); //Swipedistance


                if (couldBeSwipe && swipeTime < maxSwipeTime && swipeDist > minSwipeDist)
                {
                    // It's a swiiiiiiiiiiiipe!
                    couldBeSwipe = false; //<-- Otherwise this part would be called over and over again.

                    if (Mathf.Sign(touch.position.x - startPos.x) == 1f)
                    { //Swipe-direction, either 1 or -1.

                        //Right-swipe
                        Debug.Log("Right Swipe");
                        this.enabled = false;
                    }
                    else
                    {
                        Debug.Log("Left Swipe");
                        //Left-swipe
                    }
                }
            }
            yield return null;
        }

    }
}
